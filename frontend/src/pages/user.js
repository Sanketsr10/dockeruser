import {useState, useEffect} from 'react'
import axios from 'axios'

const UserPage = () =>{
    const url = 'http://localhost:4000/user'
    const [users, setUsers] = useState([])



    useEffect(()=>{
        getUsers()
    }, [])


    const getUsers = () =>{
        axios.get(url).then((response) => {
            const result = response.data
            console.log(result['data'])
            if(result['status'] == 'success'){
                setUsers(result['data'])
            }
        })
    }

    return(
        <div>
            <h1>user List</h1>
            <ul className="list-group">
                {users.map((user)=>{
                    return <li className="list-group-item">{user['name']}</li>
                })}
            </ul>
        </div>
    )

}

export default UserPage